class LoadingScene extends BaseScene {
	public constructor() {
		super();
	}

	// 进入屏幕
	public onEnter(): void {
		super.onEnter();

		// 添加该scene使用的层级
		this.addLayer(LayerManager.UI_Main);

		// 初始化打开Loading界面
		App.ViewManager.open(ViewConst.Loading);
	}

	// 退出scene调用
	public onExit(): void {
		super.onExit();
	}
}