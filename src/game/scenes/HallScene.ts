class HallScene extends BaseScene {
	public constructor() {
		super();
	}

	// 当进入场景
	public onEnter():void{
		super.onEnter();
		this.addLayer(LayerManager.UI_Main);
		this.addLayer(LayerManager.UI_Popup);
        // 初始化打开Hall界面
		App.ViewManager.open(ViewConst.Hall);
	}

    // 当退出场景
	public onExit():void{
		super.onExit();
	}
}