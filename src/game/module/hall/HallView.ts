class HallView extends BaseEuiView {
	private m_btn_niuniu: eui.Button;
	private m_btn_addRoom: eui.Button;
	public constructor(_controller: BaseController, _parent: eui.Group) {
		super(_controller, _parent);
		this.skinName = "resource/skins/HallUISkin.exml";
	}

	// 对面板进行初始化，用于之类继承
	public initUI(): void {
		super.initUI();
		// 对于大厅主按钮的一些监听
		this.m_btn_niuniu.addEventListener(egret.TouchEvent.TOUCH_END, this.OnClickBtnNiuNiu, this);
		this.m_btn_addRoom.addEventListener(egret.TouchEvent.TOUCH_END, this.OnClickBtnAddRoom, this);
	}

	// 对面板数据的初始化，用于子类继承
	public initData(): void {
		super.initData();
	}

	// 面板开启执行函数，用于子类继承
	public open(...param: any[]): void {
		super.open(param);
	}

	// 面板关闭执行函数，用于子类继承
	public close(...param: any[]): void {
		super.close(param);
	}

	// 当点击牛牛按钮
	private OnClickBtnNiuNiu(): void {

	}

	// 当点击加入房间按钮
	private OnClickBtnAddRoom(): void {
		console.log("OnClickBtnAddRoom");
		App.ViewManager.open(ViewConst.AddRoom);
	}
}