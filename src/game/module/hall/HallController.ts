class HallController extends BaseController {
	private hallModel: HallModel;
	private hallProxy: HallProxy;
	private hallView: HallView;
	public constructor() {
		super();

		this.hallModel = new HallModel(this);
		this.hallView = new HallView(this, LayerManager.UI_Main);
		App.ViewManager.register(ViewConst.Hall, this.hallView);

		this.hallProxy = new HallProxy(this);
	}
}