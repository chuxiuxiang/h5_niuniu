class LoadingView extends BaseEuiView {
	public constructor(_controller: BaseController, _parent: eui.Group) {
		super(_controller, _parent);
		this.skinName = "resource/skins/LoadingUISkin.exml";
	}

	public m_txtMsg: eui.Label;

	public setProgress(current: number, total: number): void {
		egret.log("current : " + current + " total : " + total);
		this.m_txtMsg.text = "资源加载中..." + current + "/" + total;
	}
}