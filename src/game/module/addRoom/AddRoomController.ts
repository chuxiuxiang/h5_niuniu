class AddRoomController extends BaseController{
	private addRoomModel: AddRoomModel;
	private addRoomProxy: AddRoomProxy;
	private addRoomView: AddRoomView;
	public constructor() {
		super();

		this.addRoomModel = new AddRoomModel(this);
		this.addRoomProxy = new AddRoomProxy(this);
		this.addRoomView = new AddRoomView(this, LayerManager.UI_Popup);
		App.ViewManager.register(ViewConst.AddRoom, this.addRoomView);

		// 注册c2s消息
		this.registerFunc(AddRoomConst.ADDROOM_C2S, this.onInPutPass, this);
	}

	private onInPutPass(pass:string):void{
        egret.log("message onInPutPass pass : " + pass);
	}
}