class AddRoomView extends BaseEuiView {
	private m_btn_close:eui.Button;
    private m_btn_n0:eui.Button;
	private m_btn_n1:eui.Button;
	private m_btn_n2:eui.Button;
	private m_btn_n3:eui.Button;
	private m_btn_n4:eui.Button;
	private m_btn_n5:eui.Button;
	private m_btn_n6:eui.Button;
	private m_btn_n7:eui.Button;
	private m_btn_n8:eui.Button;
	private m_btn_n9:eui.Button;

	private m_btn_r:eui.Button;
	private m_btn_d:eui.Button;

	private m_input0:eui.Label;
	private m_input1:eui.Label;
	private m_input2:eui.Label;
	private m_input3:eui.Label;
	private m_input4:eui.Label;
	private m_input5:eui.Label;
	private m_input6:eui.Label;

	private inputIndex:number = 0;
    private inputPassStr : string = "";
	public constructor(_controller: BaseController, _parent: eui.Group) {
		super(_controller, _parent);
		this.skinName = "resource/skins/AddRoomUISkin.exml";
	}

	// 对面版进行初始化，用于子类继承
	public initUI(): void {
		super.initUI();
		
		this.m_btn_close.addEventListener(egret.TouchEvent.TOUCH_END,this.closeBtnClickHandler,this);
		this.m_btn_n0.addEventListener(egret.TouchEvent.TOUCH_END,this.onClickPassBtn_0,this);
		this.m_btn_n1.addEventListener(egret.TouchEvent.TOUCH_END,this.onClickPassBtn_1,this);
		this.m_btn_n2.addEventListener(egret.TouchEvent.TOUCH_END,this.onClickPassBtn_2,this);
		this.m_btn_n3.addEventListener(egret.TouchEvent.TOUCH_END,this.onClickPassBtn_3,this);
		this.m_btn_n4.addEventListener(egret.TouchEvent.TOUCH_END,this.onClickPassBtn_4,this);
		this.m_btn_n5.addEventListener(egret.TouchEvent.TOUCH_END,this.onClickPassBtn_5,this);
		this.m_btn_n6.addEventListener(egret.TouchEvent.TOUCH_END,this.onClickPassBtn_6,this);
		this.m_btn_n7.addEventListener(egret.TouchEvent.TOUCH_END,this.onClickPassBtn_7,this);
		this.m_btn_n8.addEventListener(egret.TouchEvent.TOUCH_END,this.onClickPassBtn_8,this);
		this.m_btn_n9.addEventListener(egret.TouchEvent.TOUCH_END,this.onClickPassBtn_9,this);
      
	    this.m_btn_r.addEventListener(egret.TouchEvent.TOUCH_END,this.initPassInput,this);
        this.m_btn_d.addEventListener(egret.TouchEvent.TOUCH_END,this.removePass,this);
		this.initPassInput();
	}

	private closeBtnClickHandler(e:egret.TouchEvent):void{
        App.ViewManager.closeView(this);
    }

	// 对面板数据的初始化，用于子类继承
	public initData(): void {
		super.initData();
	}

	// 面板开启执行函数，用于子类继承
	public open(...param: any[]): void {
		super.open(param);
	}

	// 面板关闭执行函数，用于子类继承
	public close(...param: any[]): void {
		super.close(param);
		this.initPassInput();
	}

	// 初始化输入的密码
	private initPassInput():void{
		this.m_input0.text = "";
		this.m_input1.text = "";
		this.m_input2.text = "";
		this.m_input3.text = "";
		this.m_input4.text = "";
		this.m_input5.text = "";
		this.m_input6.text = "";
		this.inputIndex = 0;
	}

	private removePass():void{
		
		var index:number = this.inputIndex -1;
		//egret.log("this.inputIndex : " + this.inputIndex + " index : " + index);
		switch(index)
		{
			case 0:
			this.m_input0.text = "";
			break;

			case 1:
			this.m_input1.text = "";
			break;

			case 2:
			this.m_input2.text = "";
			break;

			case 3:
			this.m_input3.text = "";
			break;

			case 4:
			this.m_input4.text = "";
			break;

			case 5:
			this.m_input5.text = "";
			break;

			case 6:
			this.m_input6.text = "";
			break;
		}
		this.inputIndex -= 1;
		if(this.inputIndex < 0){
			this.inputIndex = 0;
		}
		    
	}

	// 点击密码输入按钮
	private onClickPassBtn_0(e:egret.TouchEvent):void{
		this.onInputPass("0");
	}

	private onClickPassBtn_1(e:egret.TouchEvent):void{
		this.onInputPass("1");
	}

	private onClickPassBtn_2(e:egret.TouchEvent):void{
		this.onInputPass("2");
	}

	private onClickPassBtn_3(e:egret.TouchEvent):void{
		this.onInputPass("3");
	}

	private onClickPassBtn_4(e:egret.TouchEvent):void{
		this.onInputPass("4");
	}

	private onClickPassBtn_5(e:egret.TouchEvent):void{
		this.onInputPass("5");
	}

	private onClickPassBtn_6(e:egret.TouchEvent):void{
		this.onInputPass("6");
	}

	private onClickPassBtn_7(e:egret.TouchEvent):void{
		this.onInputPass("7");
	}

	private onClickPassBtn_8(e:egret.TouchEvent):void{
		this.onInputPass("8");
	}

	private onClickPassBtn_9(e:egret.TouchEvent):void{
		this.onInputPass("9");
	}

    
	private onInputPass(pass:string):void
	{
		switch(this.inputIndex)
		{
			case 0:
			this.m_input0.text = pass;
			break;

			case 1:
			this.m_input1.text = pass;
			break;

			case 2:
			this.m_input2.text = pass;
			break;

			case 3:
			this.m_input3.text = pass;
			break;

			case 4:
			this.m_input4.text = pass;
			break;

			case 5:
			this.m_input5.text = pass;
			break;

			case 6:
			this.m_input6.text = pass;
			break;
		}
		this.inputIndex++;
        this.inputPassStr += pass;
		if(this.inputIndex >= 7)
		{
			egret.log("lock : ");
			//CommonUtils.lock();
			this.applyFunc(AddRoomConst.ADDROOM_C2S, this.inputPassStr);
		}
	}
}