class LoginController extends BaseController {
	// 本模块的数据储存
	private loginModel: LoginModel;
	private loginView: LoginView;
	private loginProxy: LoginProxy;
	public constructor() {
		super();
		// 初始化model
		this.loginModel = new LoginModel(this);
		// 初始化ui
		this.loginView = new LoginView(this, LayerManager.UI_Main);
		App.ViewManager.register(ViewConst.Login, this.loginView);

		// 初始化proxy
		this.loginProxy = new LoginProxy(this);

		// 注册模块间、模块内部事件监听

		// 注册c2s消息
		this.registerFunc(LoginConst.LOGIN_C2S, this.onLogin, this);

		// 注册是s2c消息
		this.registerFunc(LoginConst.LOGIN_S2C, this.loginSuccess, this);

	}

	// 请求登录处理
	private onLogin(userName: string, pwd: string): void {
		egret.log("onLogin userName : " + userName + " pwd : " + pwd);
		//this.loginProxy.login(userName, pwd);

         App.ViewManager.close(ViewConst.Login);
         App.ViewManager.open(ViewConst.Loading);
		 //加载资源
        var groupName: string = "preload";
        var subGroups: Array<string> = ["preload_common", "preload_addroom"];
        App.ResourceUtils.loadGroups(groupName, subGroups, this.onResourceLoadComplete, this.onResourceLoadProgress, this);
	}

	/**
     * 资源组加载完成
     */
    private onResourceLoadComplete(): void {
        App.SceneManager.runScene(SceneConsts.Hall);
    }

	/**
     * 资源组加载进度
     */
    private onResourceLoadProgress(itemsLoaded: number, itemsTotal: number): void {
		console.log("LoginController itemsLoaded : " + itemsLoaded + " itemsTotal : " + itemsTotal)
        App.ControllerManager.applyFunc(ControllerConst.Loading, LoadingConst.SetProgress, itemsLoaded, itemsTotal);
    }

	// 登录成功处理
	private loginSuccess(userInfo: any): void {
		//保存数据
		this.loginModel.userInfo = userInfo;
		//本模块UI处理
		this.loginView.loginSuccess();
		//UI跳转
		App.ViewManager.close(ViewConst.Login);

	}
}