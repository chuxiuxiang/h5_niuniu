class LoginView extends BaseEuiView {
	private m_btn_weixin: eui.Button;
	private m_btn_youke: eui.Button;

	public constructor(_controller: BaseController, _parent: eui.Group) {
		super(_controller, _parent);
		this.skinName = "resource/skins/LoginUISkin.exml";
	}

	// 对面版进行初始化，用于子类继承
	public initUI(): void {
		super.initUI();
		this.m_btn_weixin.addEventListener(egret.TouchEvent.TOUCH_END, this.onWinxinLogin, this);
		this.m_btn_youke.addEventListener(egret.TouchEvent.TOUCH_END, this.onYoukeLogin, this);
	}

	// 对面板数据的初始化，用于子类继承
	public initData(): void {
		super.initData();
	}

	// 面板开启执行函数，用于子类继承
	public open(...param: any[]): void {
		super.open(param);
	}

	// 面板关闭执行函数，用于子类继承
	public close(...param: any[]): void {
		super.close(param);
	}

	// 请求登录处理
	private onWinxinLogin(): void {
		egret.log("onWinxinLogin");
		var userName: string = "you name!";
		var pwd: string = "123456";

		//运行基础检测
		if (userName == null || userName.length == 0 || pwd == null || pwd.length == 0) {
			return;
		}

		this.applyFunc(LoginConst.LOGIN_C2S, userName, pwd);
	}

	private onYoukeLogin(): void {
		// egret.log("onYoukeLogin");
		// var userName: string = "you name!";
		// var pwd: string = "123456";

		// //运行基础检测
		// if (userName == null || userName.length == 0 || pwd == null || pwd.length == 0) {
		// 	return;
		// }

		 this.applyFunc(LoginConst.LOGIN_C2S, "游客", "666");
		//App.SceneManager.runScene(SceneConsts.Hall);
	}

	public loginSuccess(): void {
		//  登录成功处理
		egret.log("loginSuccess");
	}
}