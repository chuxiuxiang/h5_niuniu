class LoginProxy extends BaseProxy {
	public constructor(_controller: BaseController) {
		super(_controller);

		// 注册从服务器返回消息的监听
		this.receiveServerMsg(HttpConst.USER_LOGIN, this.loginSuccess, this);
	}

	// 用户登录成功返回
	public loginSuccess(obj: any): void {
		this.applyFunc(LoginConst.LOGIN_S2C, obj);
	}

	// 用户登录
	public login(userName: string, pwd: string): void {
		var paramObj: any = {
			"uName": userName,
			"uPass": pwd
		};
		this.sendHttpMsg(HttpConst.USER_LOGIN, paramObj);
	}
}